package ija.project;

import java.util.List;

public class Data {
    private List<Coordinate> coordinates;
    private List<Stop> stops;
    private List<Vehicle> vehicles;

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    private List<Line> lines;
    private List<Street> streets;

    private Data(){
    }

    public Data(List<Coordinate> coordinates, List<Street> streets, List<Line> lines) {
        this.coordinates = coordinates;
        this.streets = streets;
        this.lines = lines;
    }

    public Data(List<Stop> stops) { this.stops = stops; }

    public List<Street> getStreets() {
        return streets;
    }


    public List<Stop> getStops() {
        return stops;
    }

    public List<Line> getLines() {
        return lines;
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    @Override
    public String toString() {
        return "Data{" +
                "coordinates=" + coordinates +
                ", stops=" + stops +
                ", lines=" + lines +
                ", streets=" + streets +
                '}';
    }
}
