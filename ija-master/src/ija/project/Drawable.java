package ija.project;


import javafx.scene.shape.Shape;

import java.util.List;

public interface Drawable {
    List<Shape> getShapes();
    void setShapes();
}
