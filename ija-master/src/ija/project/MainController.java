package ija.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.net.URL;
import java.time.LocalTime;
import java.util.*;

public class MainController implements Initializable{

    @FXML
    private Pane content;

    @FXML
    private Text speedDisplayText;

    @FXML
    private javafx.scene.shape.Line borderLine1;
    @FXML
    private javafx.scene.shape.Line borderLine2;
    @FXML
    private ComboBox<String> hoursInput;

    @FXML
    private ComboBox<String> minutesInput;

    @FXML
    private ComboBox<Integer> diversionMinutesInput;

    @FXML
    private Text timeDisplay;
    @FXML
    private Text hoursDisplay;

    @FXML
    private Text colonTime;

    @FXML
    private Button confirmTime;

    @FXML
    private Text trafficDisplayText;

    @FXML
    private Text labelSchedule;

    @FXML
    private TextArea ScheduleTime;


    private List<Drawable> elements = new ArrayList<>();
    private List<TimeUpdate> updates = new ArrayList<>();
    private Timer timer;
    private List<Line> lines = new ArrayList<>();
    private List<Street> streets = new ArrayList<>();
    private LocalTime time = LocalTime.now();
    private List<String> listWithHours = new ArrayList<>();
    private ObservableList<String> hoursList = FXCollections.observableList(listWithHours);
    private List<String> listWithMinutes = new ArrayList<>();
    private ObservableList<String> minutesList = FXCollections.observableList(listWithMinutes);
    private int speed = 1;
    private int maxSpeed = 4;
    private int minSpeed = 1;
    private double traffic = 0;
    private boolean detouring = false;
    private List<Street> detourStreets = new ArrayList<>();
    private Street detouredStreet;
    private double diversionTime = 0;
    private List<Integer> listWithMinutesDiversion = new ArrayList<>();
    private ObservableList<Integer> minutesListDiversion = FXCollections.observableList(listWithMinutesDiversion);

    @FXML
    public void DisplayDepartures() {
        labelSchedule.setVisible(true);
        ScheduleTime.setVisible(true);
        for (Line line : getLines()) {
            for(Vehicle vehicle : line.getVehicles()) {
                if ((vehicle.isSelected())) {
                    ScheduleTime.setText(vehicle.getDepartureTimes());
                    vehicle.setDepartureTimes("");
                }
            }
        }
    }

    public void unhighighlightLines() {
        for (Line line : lines) {
            for (Street street : line.getStreets()) {
                street.setHighlighted(false);
                street.unHighlight();
            }
        }
    }

    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }

    @FXML
    public void onMakeDetour() {
        for (Street street : streets) {
            if (street.isSelected()) {
                street.setDetouring(true);
                setDetouring(true);
            }
        }
    }

    public Street getDetouredStreet() {
        return detouredStreet;
    }

    public void setDetouredStreet(Street detouredStreet) {
        this.detouredStreet = detouredStreet;
    }

    public boolean isDetouring() {
        return detouring;
    }

    public void setDetouring(boolean detouring) {
        this.detouring = detouring;
    }

    public List<Street> getDetourStreets() {
        return detourStreets;
    }

    public void addDetourStreets(Street detourStreets) {
        this.detourStreets.add(detourStreets);
    }

    @JsonIgnore
    public TextArea getScheduleTime() {
        return ScheduleTime;
    }

    @JsonIgnore
    public void setScheduleTime(TextArea scheduleTime) {
        ScheduleTime = scheduleTime;
    }

    @FXML
    public void onConfirmDetour() {
        if (diversionMinutesInput.getValue() != null) {
            diversionTime = diversionMinutesInput.getValue();
        }
        for (Line line : lines)
        {
            removeDetouredStops(line);
            substituteDetouredStreets(line);
            setDetouring(false);
        }
        detourStreets.clear();
    }

    @FXML
    public void DisplayTime(double time)
    {
        int minutes = (int) time;
        int hours = 0;
        if (minutes >= 60) {
            hours = minutes / 60;
            minutes = minutes % 60;
        }
        String stringHours = Integer.toString(hours);
        String stringMinutes = Integer.toString(minutes);

        if (hours < 10) {
            stringHours = "0".concat(stringHours);
        }
        if (minutes < 10) {
            stringMinutes = "0".concat(stringMinutes);
        }

        hoursDisplay.setText(stringHours);

        timeDisplay.setText(stringMinutes);
    }

    public void addLine(Line line)
    {
        this.lines.add(line);
    }

    @FXML
    private void onTimeClicked() {
        if (!colonTime.visibleProperty().getValue()) {
            colonTime.setVisible(true);
            hoursInput.setVisible(true);
            minutesInput.setVisible(true);
            confirmTime.setVisible(true);
            borderLine1.setVisible(true);
            borderLine2.setVisible(true);
        }
        else if (colonTime.visibleProperty().getValue()) {
            colonTime.setVisible(false);
            hoursInput.setVisible(false);
            minutesInput.setVisible(false);
            confirmTime.setVisible(false);
            borderLine1.setVisible(false);
            borderLine2.setVisible(false);
        }
    }

    private void removeDetouredStops(Line line)
    {
        line.getStops().removeIf(stop -> stop.getStreet().equals(detouredStreet));
    }

    private void substituteDetouredStreets(Line line)
    {
        int index = 0;
        List<Street> firstHalf = new ArrayList<>();
        List<Street> secondHalf = new ArrayList<>();
        for (Street street : line.getStreets()) {
            if (street.equals(detouredStreet)) {
                firstHalf = line.getStreets().subList(0, index);
                secondHalf = line.getStreets().subList(index + 1, line.getStreets().size());
                List<Street> streets = new ArrayList<>();
                streets.addAll(firstHalf);
                invertDetourStreets(line);
                //todo 2: set delay on first stop after detour based on direction
                setDelayOnDetour(secondHalf, line);
                streets.addAll(detourStreets);
                streets.addAll(secondHalf);
                line.setStreets(streets);
                break;
            }
            index++;
        }
    }

    private void invertDetourStreets(Line line)
    {
        //fixme followuje vzdy, treba opravit
        if(detourStreets.get(0).follows(detouredStreet)) {
            return;
        }

        Collections.reverse(detourStreets);
    }

    private void setDelayOnDetour(List<Street> streets, Line line)
    {
        for (Street street : streets) {
            for (Stop stop : street.getStops()){
                for (Stop lineStop : line.getStops()) {
                    if (stop.equals(lineStop)) {
                        stop.incrementStopTime(diversionTime);
                    }
                }
            }
        }
    }

    @FXML
    private void onSetTrafficTo0() {
        traffic = 0;
        setStreetDelay(traffic);
        trafficDisplayText.setText("-");
    }

    @FXML
    private void onSetTrafficTo1() {
        traffic = 1;
        setStreetDelay(traffic);
        trafficDisplayText.setText("+" + "1");
    }

    @JsonIgnore
    public Text getTrafficDisplayText() {
        return trafficDisplayText;
    }

    @JsonIgnore
    public void setTrafficDisplayText(Text trafficDisplayText) {
        this.trafficDisplayText = trafficDisplayText;
    }

    @FXML
    private void onSetTrafficTo3() {
        traffic = 3;
        setStreetDelay(traffic);
        trafficDisplayText.setText("+" + "3");
    }

    @FXML
    private void onSetTrafficTo5() {
        traffic = 5;
        setStreetDelay(traffic);
        trafficDisplayText.setText("+" + "5");
    }

    private void setStreetDelay(double traffic) {
        for (Street street : streets) {
            if(street.isSelected()) {
                street.setAlreadyDelayed(false);
                street.setDelay(traffic);
            }
        }
    }

    @FXML
    private void onChangeTime() {
        String stringHours = hoursInput.getValue();
        String stringMinutes = minutesInput.getValue();
        if (stringHours == null) {
            stringHours = "00";
        }
        if (stringMinutes == null) {
            stringMinutes = "00";
        }
        double time = Double.parseDouble(stringHours)*60 + Double.parseDouble(stringMinutes);

        for (Line line : this.lines) {
            line.setTime(time);
        }
    }

    @FXML
    private void onSpeedUp() {
        if (!(speed >= maxSpeed)) {
            speed++;
            String displaySpeed = Integer.toString(speed);
            speedDisplayText.setText(displaySpeed + "x");
        }

        for (Line line : this.lines) {
            line.setTimeIncrement(speed);
        }
    }

    @FXML
    private void onSpeedDown() {
        if (speed > minSpeed) {
            speed--;
            String displaySpeed = Integer.toString(speed);
            speedDisplayText.setText(displaySpeed + "x");
        }

        for (Line line : this.lines) {
            line.setTimeIncrement(speed);
        }
    }

    @FXML
    private void onZoom(ScrollEvent event) {
        event.consume();
        double zoom = event.getDeltaY() > 0 ? 1.2 : 0.9;
        content.setScaleX(zoom * content.getScaleX());
        content.setScaleY(zoom * content.getScaleY());
        content.layout();

    }


    public void setElements(List<Drawable> elements) {
        this.elements = elements;
        for (Drawable drawable : elements) {
            content.getChildren().addAll(drawable.getShapes());
            if(drawable instanceof TimeUpdate) {
                updates.add((TimeUpdate) drawable);
            }
        }

    }

    public void addUpdatable(TimeUpdate update)
    {
        updates.add(update);
    }

    public void startTime(float scale) {
        timer = new Timer(false);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                double simTime = 0;
                time = time.plusSeconds(1);
                for (TimeUpdate update : updates) {
                    if(update instanceof Line) {
                        simTime = ((Line) update).getTime();
                    }
                    update.update(time);
                }
                DisplayTime(simTime);
            }
        }, 0, (long) (1000 / scale));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String stringSpeed = Integer.toString(speed);
        speedDisplayText.setText(stringSpeed + "x");
        hoursInput.setItems(hoursList);
        minutesInput.setItems(minutesList);
        // for hours options
        for (int i = 0; i <= 1; i++) {
            String str1 = Integer.toString(i);
            for (int j = 0; j <= 9; j++) {
                String str2 = Integer.toString(j);
                str2 = str1.concat(str2);
                listWithHours.add(str2);
            }
        }
        for (int i = 20; i <= 24; i++) {
            listWithHours.add(Integer.toString(i));
        }
        // for minutes options
        for (int i = 0; i <= 5; i++) {
            String str1 = Integer.toString(i);
            for (int j = 0; j <= 9; j++) {
                String str2 = Integer.toString(j);
                str2 = str1.concat(str2);
                listWithMinutes.add(str2);
            }
        }
        //combobox diversion
        diversionMinutesInput.setItems(minutesListDiversion);
        for (int i = 0; i <= 59; i++) {
            //double j = i;
            listWithMinutesDiversion.add(i);
        }
    }
}
