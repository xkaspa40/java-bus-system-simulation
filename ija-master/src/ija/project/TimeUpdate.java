package ija.project;

import java.time.LocalTime;

public interface TimeUpdate {
    void update(LocalTime time);
}
