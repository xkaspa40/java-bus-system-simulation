package ija.project;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Stop implements Drawable {
    private String id;

    public String getId() {
        return id;
    }

    private double[] stopTimes;
    private double[] stopTimesImmutable;
    private static final int WIDTH = 12;
    private String name;
    private Street street;
    private Coordinate position;
    @JsonIgnore
    private List<Shape> shapes;

    public String getName() {
        return name;
    }

    public Stop(double[] stopTime, Coordinate position, Street street, String name) {
        this.stopTimes = stopTime;
        this.position = position;
        this.street = street;
        this.name = name;
    }

    public double[] getStopTimes() {
        return stopTimes;
    }

    public void incrementStopTime(double increment) {
        if (stopTimesImmutable == null) {
            stopTimesImmutable = stopTimes.clone();
        }
        for(int i = 0; i < stopTimes.length; i ++) {
            //using "immutable" copy so we can reset and change delays on the same stop
            stopTimes[i] += increment;
            if (increment == 0) {
                stopTimes[i] = stopTimesImmutable[i];
            }
        }
    }

    public Street getStreet() {
        return street;
    }


    @Override
    public String toString() {
        return "Stop{" +
                "stopTime=" + stopTimes +
                ", street=" + street +
                ", position=" + position +
                ", gui=" + shapes +
                '}';
    }

    @Override
    public List<Shape> getShapes()
    {
        return shapes;
    }

    public boolean isOnStop(Vehicle vehicle)
    {
        return vehicle.getPosition().equals(this.position);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stop stop = (Stop) o;
        return stop.name.equals(name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public void setShapes()
    {
        shapes = new ArrayList<>();
        Rectangle r = new Rectangle(position.getX() - WIDTH/2, position.getY(), WIDTH, WIDTH);
        r.setFill(Color.RED);
        shapes.add(r);
        Text name = new Text(getName());
        name.setFill(Color.RED);
        name.setX(position.getX() - name.getLayoutBounds().getWidth()/2);
        name.setY(position.getY() + name.getLayoutBounds().getHeight() / 2 + 20);
        shapes.add(name);
    }

    private Stop() {};

    public Coordinate getPosition()
    {
        return position;
    }
}
