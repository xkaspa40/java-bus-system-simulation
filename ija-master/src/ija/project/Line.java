package ija.project;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Line implements TimeUpdate {
    private List<Street> streets;
    private List<Stop> stops;
    private double timeIncrement = 1;
    private double time = currentTime();
    //private double time = 0;
    @JsonIgnore
    private List<Vehicle> vehicles;
    private boolean invertedDetour = false;
    private int vehicleCount;
    private String number;
    private boolean alreadyDelayed = false;

    private Line() {
    }

    public static double currentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatHours = new SimpleDateFormat("HH");
        SimpleDateFormat formatMinutes = new SimpleDateFormat("mm");

        String stringHours = formatHours.format(cal.getTime());
        String stringMinutes = formatMinutes.format(cal.getTime());

        double hours = Double.parseDouble(stringHours) * 60;
        double minutes = Double.parseDouble(stringMinutes);

        return hours + minutes;
    }

    @Override
    public String toString() {
        return "Line{" +
                "streets=" + streets +
                ", vehicles=" + vehicles +
                ", vehicleCount=" + vehicleCount +
                ", number='" + number + '\'' +
                '}';
    }

    public boolean isInvertedDetour() {
        return invertedDetour;
    }

    public void setInvertedDetour(boolean invertedDetour) {
        this.invertedDetour = invertedDetour;
    }

    public void setTimeIncrement(double increment)
    {
        this.timeIncrement = increment;
    }

    public int getVehicleCount() {
        return vehicleCount;
    }

    public Line(List<Street> streets, int vehicleCount, String number, List<Stop> stops) {
        this.number = number;
        this.streets = streets;
        this.vehicles = new ArrayList<>();
        this.vehicleCount = vehicleCount;
        this.stops = stops;
    }

    public double getTime()
    {
        return this.time;
    }

    public void createVehicles(Coordinate position, MainController controller) {
        //MainController controller;
        vehicles = new ArrayList<>();
        for (int i = 0; i < vehicleCount; i++) {
            vehicles.add(new Vehicle(streets.get(0).getStart(), this, number, i, controller));
        }

    }

    public Stop getFirstStop() {
        return this.stops.get(0);
    }

    public void setTime(double time) {
        this.time = time;
    }

    public boolean isAlreadyDelayed() {
        return alreadyDelayed;
    }


    @Override
    public void update(LocalTime time) {
        //midnight
        if (this.time >= 1440 ) {
            this.time = 0;
        }
        for (Vehicle inactiveVehicle : getInactiveVehicles()) {
            double firstStopTime = getFirstStop().getStopTimes()[inactiveVehicle.getStopTimeIndex()];
            double lastStopTime = getLastStop().getStopTimes()[inactiveVehicle.getStopTimeIndex()];
            if( firstStopTime <= this.time && this.time <= lastStopTime) {
                inactiveVehicle.activate();
            }
        }
        for (Vehicle vehicle : getActiveVehicles()) {

            double firstStopTime = getFirstStop().getStopTimes()[vehicle.getStopTimeIndex()];
            if (firstStopTime > this.time) {
                vehicle.deactivate();
            }
            List<Stop> passedStops = vehicle.getPassedStops();
            double arrivalTime = -1;
            if(passedStops.size() != 0) {
                arrivalTime = passedStops.get(passedStops.size() - 1).getStopTimes()[vehicle.getStopTimeIndex()];
            }
            // vehicle waits for one minute at stop
            if (arrivalTime != -1 && this.time == arrivalTime + 1) {
                continue;
            }

            try {
                refreshStopsByTime(this.time, vehicle);
                Stop nextStop = this.getNextStop(vehicle);
                // if time is so hastened we miss the stop
                double nextTime = nextStop.getStopTimes()[vehicle.getStopTimeIndex()];
                if (nextTime < this.time) {
                    double newStopTime = 0;
                    do {
                        vehicle.addPassedStop(nextStop);
                        vehicle.transformShape(nextStop.getPosition());
                        vehicle.setPosition(nextStop.getPosition());
                        nextStop = this.getNextStop(vehicle);
                        newStopTime = nextStop.getStopTimes()[vehicle.getStopTimeIndex()];
                    } while (newStopTime < this.time);
                }

                if (nextStop.getStopTimes()[vehicle.getStopTimeIndex()] == this.time) {
                    moveToStop(vehicle, nextStop);
                }

                if (nextStop.getStopTimes()[vehicle.getStopTimeIndex()] > this.time) {
                    List<Street> streetsToNextStop = getStreetsToNextStop(nextStop, vehicle);
                    setDelays(vehicle);
                    double distance = getDistanceToNextStop(vehicle.getPosition(), streetsToNextStop, nextStop);
                    double step = getStep(distance, nextStop.getStopTimes()[vehicle.getStopTimeIndex()] - this.time);
                    doStep(vehicle, step, distance, streetsToNextStop, nextStop);

                }
            } catch (NullPointerException ex) {
                vehicle.deactivate();
            }

        }
        this.time += timeIncrement;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public String getNumber() {
        return number;
    }

    private Stop getNextStop(Vehicle vehicle) {
        for (Stop stop : this.stops) {
            if (vehicle.getPassedStops().contains(stop)) {
                continue;
            }
            return stop;
        }

        return null;
    }

    private void setDelays(Vehicle vehicle) {
        boolean notBehindDelayed = true;
        for (Street street : streets) {
            if (street.isOnStreet(vehicle.getPosition())) {
                if ( ! street.isAlreadyDelayed()) {
                    for (Stop stop : getStopsFromStreet(street)) {
                        stop.incrementStopTime(street.getDelay());
                    }
                    street.setAlreadyDelayed(true);
                }
            }
        }
    }

    private List<Vehicle> getInactiveVehicles() {
        List<Vehicle> inactiveVehicles = new ArrayList<>();
        for (Vehicle vehicle : this.vehicles) {
            if (!vehicle.isActive()) {
                inactiveVehicles.add(vehicle);
            }
        }

        return inactiveVehicles;
    }

    private List<Vehicle> getActiveVehicles() {
        List<Vehicle> activeVehicles = new ArrayList<>();
        for (Vehicle vehicle : this.vehicles) {
            if (vehicle.isActive()) {
                activeVehicles.add(vehicle);
            }
        }

        return activeVehicles;
    }

    private List<Stop> getStopsFromStreet(Street delayedStreet)
    {
        boolean afterStreet = false;
        List <Stop> nextStops = new ArrayList<>();
        for (Stop stop : stops) {
            if ( ! delayedStreet.isOnStreet(stop.getPosition()) && ! afterStreet) {
                continue;
            }
            afterStreet = true;
            nextStops.add(stop);
        }
        if (nextStops.isEmpty()) {
            boolean tryNext = false;
            for (Street street : streets) {

                if (tryNext) {
                    return getStopsFromStreet(street);
                }

                if (street.equals(delayedStreet)) {
                    tryNext = true;
                }
            }
        }

        return nextStops;
    }

    private List<Street> getStreetsToNextStop(Stop stop, Vehicle vehicle) {
        List<Street> streetsToNextStop = new ArrayList<>();
        Street actualVehicleStreet = vehicle.getVehicleStreet();
        for (Street street : this.streets) {
            if (!street.equals(actualVehicleStreet) && streetsToNextStop.isEmpty()) {
                continue;
            }
            streetsToNextStop.add(street);
            if (street.getStops() == null) {
                continue;
            }
            if (street.getStops().contains(stop)) {
                break;
            }
        }
        return streetsToNextStop;
    }

    private double getDistanceToNextStop(Coordinate vehiclePosition, List<Street> streetsToNextStop, Stop nextStop) {
        ArrayList<Double> distances = getDistances(vehiclePosition, streetsToNextStop, nextStop);
        double distance = 0;
        for (Double streetLength : distances) {
            distance += streetLength;
        }

        return distance;
    }

    private ArrayList<Double> getDistances(Coordinate vehiclePosition, List<Street> streetsToNextStop, Stop nextStop) {
        int i = 0;
        ArrayList<Double> distance = new ArrayList<>();
        Street penultimate = null;
        Street previous = null;
        for (Street next : streetsToNextStop) {
            if (i == 0) {
                i++;
                previous = new Street(next.getName(), next.getStart(), next.getEnd());
                continue;
            }

            // we get length of this street relative to vehicle's position on this street
            if (i == 1) {
                // streets follow like previous end -> next
                if (previous.followsEndToStart(next) || previous.followsEndToEnd(next)) {
                    distance.add(previous.getStreetLength(vehiclePosition, previous.getEnd()));
                }
                // streets follow like previous start -> next
                else if (previous.followsStartToEnd(next) || previous.followsStartToStart(next)) {
                    distance.add(previous.getStreetLength(vehiclePosition, previous.getStart()));
                }

                penultimate = new Street(previous.getName(), previous.getStart(), previous.getEnd());
                previous = new Street(next.getName(), next.getStart(), next.getEnd());
                i++;
                continue;
            }

            // length of streets without vehicle and stop are counted start to end
            distance.add(previous.getStreetLength(previous.getStart(), previous.getEnd()));

            penultimate = new Street(previous.getName(), previous.getStart(), previous.getEnd());
            previous = new Street(next.getName(), next.getStart(), next.getEnd());
        }

        //vehicle is on same street as next stop is
        if (penultimate == null) {
            distance.add(previous.getStreetLength(vehiclePosition, nextStop.getPosition()));
            return distance;
        }

        // we need to get length of street with stop
        if (penultimate.followsStartToStart(previous) || penultimate.followsEndToStart(previous)) {
            distance.add(previous.getStreetLength(previous.getStart(), nextStop.getPosition()));
        } else if (penultimate.followsStartToEnd(previous) || penultimate.followsEndToEnd(previous)) {
            distance.add(previous.getStreetLength(previous.getEnd(), nextStop.getPosition()));
        }

        return distance;
    }

    private double getStep(double distance, double time)
    {
        return distance / (time / this.timeIncrement + 1);
    }

    private void doStep(Vehicle vehicle, double step, double distanceToStop,
                        List<Street> streetsToNextStop, Stop nextstop) {
        ArrayList<Double> distances = getDistances(vehicle.getPosition(), streetsToNextStop, nextstop);

        int i = 0;
        Street previous = null;
        Street next = null;
        previous = streetsToNextStop.get(0);
        for (Street street : streetsToNextStop) {
            next = street;
            if (step >= distances.get(i)) {
                step -= distances.get(i);
                i++;
                previous = new Street(next.getName(), street.getStart(), street.getEnd());
                continue;
            }

            break;
        }

        double x, y;
        if (previous.equals(next)) {
            if (nextstop.getPosition().getX() == vehicle.getPosition().getX()) {
                x = next.getStart().getX();
                //higher Y is lower in our case
                if (nextstop.getPosition().getY() > vehicle.getPosition().getY()) {
                    y = vehicle.getPosition().getY() + step;
                } else {
                    y = vehicle.getPosition().getY() - step;
                }
            } else if (nextstop.getPosition().getY() == vehicle.getPosition().getY()) {
                if (nextstop.getPosition().getX() > vehicle.getPosition().getX()) {
                    x = vehicle.getPosition().getX() + step;
                    y = next.getStart().getY();
                } else {
                    x = vehicle.getPosition().getX() - step;
                    y = next.getStart().getY();
                }
            } else {
                if (this.fromStartToEnd(previous)) {
                    if (next.getStart().getX() == next.getEnd().getX()) {
                        x = next.getStart().getX();
                        if (next.getStart().getY() > next.getEnd().getY()) {
                            y = vehicle.getPosition().getY() - step;
                        } else {
                            y = vehicle.getPosition().getY() + step;
                        }
                    } else {
                        y = next.getStart().getY();
                        if (next.getStart().getX() > next.getEnd().getX()) {
                            x = vehicle.getPosition().getX() - step;
                        } else {
                            x = vehicle.getPosition().getX() + step;
                        }
                    }
                } else {
                    if (next.getStart().getX() == next.getEnd().getX()) {
                        x = next.getStart().getX();
                        if (next.getStart().getY() > next.getEnd().getY()) {
                            y = vehicle.getPosition().getY() + step;
                        } else {
                            y = vehicle.getPosition().getY() - step;
                        }
                    } else {
                        y = next.getStart().getY();
                        if (next.getStart().getX() > next.getEnd().getX()) {
                            x = vehicle.getPosition().getX() + step;
                        } else {
                            x = vehicle.getPosition().getX() - step;
                        }
                    }
                }
            }
        }
        //direction is start to end
        else if (previous.followsEndToStart(next) || previous.followsStartToStart(next)) {
            if (next.getStart().getX() == next.getEnd().getX()) {
                x = next.getStart().getX();
                if (next.getStart().getY() > next.getEnd().getY()) {
                    y = vehicle.getPosition().getY() - step;
                } else {
                    y = vehicle.getPosition().getY() + step;
                }
            } else {
                y = next.getStart().getY();
                if (next.getStart().getX() > next.getEnd().getX()) {
                    x = vehicle.getPosition().getX() - step;
                } else {
                    x = vehicle.getPosition().getX() + step;
                }
            }
            //direction is end to start
        } else {
            if (next.getStart().getX() == next.getEnd().getX()) {
                x = next.getStart().getX();
                if (next.getStart().getY() > next.getEnd().getY()) {
                    y = vehicle.getPosition().getY() + step;
                } else {
                    y = vehicle.getPosition().getY() - step;
                }
            } else {
                y = next.getStart().getY();
                if (next.getStart().getX() > next.getEnd().getX()) {
                    x = vehicle.getPosition().getX() + step;
                } else {
                    x = vehicle.getPosition().getX() - step;
                }
            }
        }


        Coordinate newPosition = new Coordinate(x, y);
        vehicle.transformShape(newPosition);
        vehicle.setPosition(newPosition);
    }

    private void moveToStop(Vehicle vehicle, Stop nextStop) {
        vehicle.transformShape(nextStop.getPosition());
        vehicle.setPosition(nextStop.getPosition());
        vehicle.addPassedStop(nextStop);
    }

    private boolean fromStartToEnd(Street actual) {
        int i = 0;
        Street next = null;
        for (Street street : this.streets) {
            if (street.equals(actual)) {
                next = this.streets.get(i + 1);
            }
            i++;
        }

        return actual.followsEndToStart(next) || actual.followsEndToEnd(next);
    }

    private Stop getLastStop()
    {
        return this.stops.get(this.stops.size() - 1);
    }

    private void refreshStopsByTime(double time, Vehicle vehicle)
    {
        List<Stop> stops = vehicle.getPassedStops();
        if (stops.size() == 0) return;
        List<Stop> passed = new ArrayList<>();
        for (Stop stop : stops) {
            if (stop.getStopTimes()[vehicle.getStopTimeIndex()] < time) {
                passed.add(stop);
            }
        }

        if (! passed.equals(stops)) {
            //move vehicle to last visited stop
            Coordinate newPosition;
            if (passed.isEmpty() || passed.size() == 1) {
                newPosition = this.stops.get(0).getPosition();
            }
            else {
                newPosition = passed.get(passed.size() - 1).getPosition();
            }
            vehicle.transformShape(newPosition);
            vehicle.setPosition(newPosition);
        }

        vehicle.setPassedStops(passed);
    }

}
