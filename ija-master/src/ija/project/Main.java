package ija.project;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout.fxml"));
        BorderPane root = loader.load();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        MainController controller =  loader.getController();
        List<Drawable> elements = new ArrayList<>();
        List<Street> streets = new ArrayList<>();


        YAMLFactory factory = new YAMLFactory().disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER);
        ObjectMapper mapper = new ObjectMapper(factory);
        Data data1 = mapper.readValue(new File("data/data.yml"), Data.class);
        for (Stop stop : data1.getStops()) {
            stop.getStreet().addStop(stop);
            stop.setShapes();
        }

        for (Line line: data1.getLines())
        {
            line.createVehicles(line.getFirstStop().getPosition(), controller);
            controller.addLine(line);
            controller.addUpdatable(line);
            elements.addAll(line.getVehicles());
        }

        for (Street street: data1.getStreets())
        {
            street.setShapes();
            street.setController(controller);
        }

        elements.addAll(data1.getStreets());
        elements.addAll(data1.getStops());
        controller.setStreets(data1.getStreets());
        controller.setElements(elements);
        controller.startTime(1);

        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });

    }
}
