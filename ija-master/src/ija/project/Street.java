package ija.project;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Street implements Drawable
{
    private String id;
    private String name;
    private MainController controller;
    private static final int STROKE_WIDTH = 3;
    public Coordinate getEnd() {
        return end;
    }
    private List<Stop> stops = new ArrayList<>();
    private Coordinate start;
    private boolean selected;
    private boolean highlighted;
    private boolean detouring = false;
    private boolean partOfDetour = false;
    private Coordinate end;
    private double delay = 0;
    private boolean alreadyDelayed = true;
    private List<Shape> shapes;


    public Street(String name, Coordinate start, Coordinate end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }


    public boolean isDetouring() {
        return detouring;
    }

    public void setDetouring(boolean detouring) {
        this.detouring = detouring;
        appearDetouring();
    }


    public void setController(MainController controller) {
        this.controller = controller;
    }

    public double getDelay() {
        return delay;
    }

    public void setDelay(double delay) {
        this.delay = delay;
    }

    public boolean isAlreadyDelayed() {
        return alreadyDelayed;
    }

    public void setAlreadyDelayed(boolean alreadyDelayed) {
        this.alreadyDelayed = alreadyDelayed;
    }

    public String getId() {
        return id;
    }

    public void setShapes()
    {
        this.shapes = new ArrayList<>();
        Text name = new Text(start.getX() + (Math.abs(start.getX() - end.getX()) /2) + STROKE_WIDTH,start.getY() + (Math.abs(start.getY() - end.getY())  /2) - STROKE_WIDTH, this.name);
        Line line = new Line(start.getX(), start.getY(), end.getX(), end.getY());
        line.setStrokeWidth(STROKE_WIDTH);
        this.shapes.add(name);
        this.shapes.add(line);

        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (isSelected()){
                    unmarkSelected();
                    return;
                }
                unmarkSelected();
                markSelected();
            }
        };
        //Registering the event filter
        line.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
    }

    public void addStop(Stop stop) {
        if (this.stops == null) {
            this.stops = new ArrayList<>();
        }
        this.stops.add(stop);
    }

    @JsonIgnore
    @Override
    public List<Shape> getShapes()
    {
        return this.shapes;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if ( ! (obj instanceof Street)) {
            return false;
        }

        Street street = (Street) obj;

        return this.getName().equals(street.getName());
    }

    @JsonProperty("start")
    public Coordinate getStart() {
        return start;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public double getStreetLength(Coordinate start, Coordinate end)
    {
        double startX = start.getX();
        double endX = end.getX();
        double startY = start.getY();
        double endY = end.getY();

        if (startX == endX) {
            return Math.abs(startY - endY);
        }

        return Math.abs(startX - endX);
    }

    public boolean isOnStreet(Coordinate coordinate)
    {
        if (coordinate.equals(this.getStart()) || coordinate.equals(this.getEnd())) {
            return true;
        }

        if (this.end.getX() == this.start.getX() && coordinate.getX() == this.start.getX())
        {
            if (this.start.getY() > this.end.getY())
            {
                return coordinate.getY() < this.start.getY() && coordinate.getY() > this.end.getY();
            }

            return coordinate.getY() > this.start.getY() && coordinate.getY() < this.end.getY();

        }

        if (this.end.getY() == coordinate.getY())
        {
            if (this.start.getX() > this.end.getX())
            {
                return coordinate.getX() < this.start.getX() && coordinate.getX() > this.end.getX();
            }

            return coordinate.getX() > this.start.getX() && coordinate.getX() < this.end.getX();
        }

        return false;
    }

    public boolean follows(Street street) {
        return followsEndToStart(street) || followsStartToStart(street) || followsStartToEnd(street) || followsEndToEnd(street);
    }

    public boolean followsStartToEnd(Street street)
    {
        return this.getStart().equals(street.getEnd());
    }

    public boolean followsEndToStart(Street street)
    {
        return this.getEnd().equals(street.getStart());
    }

    public boolean followsStartToStart(Street street)
    {
        return this.getStart().equals(street.getStart());
    }

    public boolean followsEndToEnd(Street street)
    {
        return this.getEnd().equals(street.getEnd());
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isHighlighted() {
        return highlighted;
    }
    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
        //System.out.println(highlighted);
    }


    @Override
    public String toString() {
        return "Street{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }

    private Street() {
    }

    public void unHighlight() {
        for (Shape shape : shapes) {
            try {
                if ( ! (shape instanceof Line)) continue;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        //shape.setHighlighted();
                        shape.setStroke(Color.BLACK);
                        shape.setStrokeWidth(STROKE_WIDTH);
                    }
                });
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Exception");
            }
        }
    }

    public void Highlight()
    {
        if (isHighlighted()){
            unHighlight();
        }
        unHighlight();
        for (Shape shape : shapes) {
            try {
                if ( ! (shape instanceof Line)) continue;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        //shape.setHighlighted();
                        shape.setStroke(Color.LIGHTSEAGREEN);
                        shape.setStrokeWidth(STROKE_WIDTH*2);
                    }
                });
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Exception");
            }
        }
        this.highlighted = true;
    }


    private void unmarkSelected()
    {
        List<Street> streets = controller.getStreets();
        for (Street street : streets) {
            if (street.isSelected()) {
                street.setSelected(false);
                controller.getTrafficDisplayText().setText("-");
                if (street.partOfDetour) continue;
                street.appearUnselected();
            }
        }
    }

    private void markSelected()
    {
        Street thisStreet = this;
        for (Shape shape : shapes) {
            try {
                if ( ! (shape instanceof Line)) continue;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if ( ! controller.isDetouring()) {
                            shape.setStroke(Color.ORANGE);
                            shape.setStrokeWidth(STROKE_WIDTH*2);
                        } else {
                            if (controller.getDetourStreets().contains(thisStreet)) return;

                            Street detouredStreet = controller.getDetourStreets().isEmpty() ? null :  controller.getDetourStreets().get(controller.getDetourStreets().size()-1);

                            if ( null == detouredStreet || follows(detouredStreet))
                            {
                                shape.setStroke(Color.NAVY);
                                thisStreet.partOfDetour = true;
                                shape.setStrokeWidth(STROKE_WIDTH * 2);
                                controller.addDetourStreets(thisStreet);
                            }
                        }
                    }
                });
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Exception");
            }
        }
        controller.getTrafficDisplayText().setText("+" + (int) delay);
        if (delay == 0) {
            controller.getTrafficDisplayText().setText("-");
        }
        this.selected = true;
    }

    private void appearUnselected()
    {
        for (Shape shape : this.getShapes()) {
            try {
                if ( ! (shape instanceof Line)) continue;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if ( ! controller.isDetouring())
                        {
                            shape.setStroke(Color.BLACK);
                            shape.setStrokeWidth(STROKE_WIDTH);
                        }
                    }
                });
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Exception");
            }
        }
    }

    private void appearDetouring()
    {
        Street thisStreet = this;
        for (Shape shape : this.getShapes()) {
            try {
                if ( ! (shape instanceof Line)) continue;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (detouring) {
                            shape.setStroke(Color.INDIANRED);
                            shape.setStrokeWidth(STROKE_WIDTH*2);
                            controller.setDetouredStreet(thisStreet);
                            return;
                        }
                        shape.setStroke(Color.BLACK);
                        shape.setStrokeWidth(STROKE_WIDTH);
                    }
                });
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Exception");
            }
        }
    }
}
