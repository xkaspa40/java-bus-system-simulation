package ija.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Vehicle implements Drawable, TimeUpdate {
    private Coordinate position;
    private String number;
    private Line line;
    private MainController controller;
    private static final int CIRCLE_RADIUS = 10;
    private int stopTimeIndex;
    private List<Stop> passedStops;
    private boolean active;
    private String DepartureTimes = "";
    private boolean selected;

    @JsonIgnore
    private List<Shape> shapes;

    private Vehicle(){}


    public Vehicle(Coordinate position, Line Line, String number, int stopTimeIndex, MainController controller) {
        this.position = position;
        this.line = Line;
        this.controller = controller;
        this.number = number;
        this.active = false;
        this.passedStops = new ArrayList<>();
        this.stopTimeIndex = stopTimeIndex;

        setShapes();
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void deactivate()
    {
        this.active = false;
        makeTransparent();
    }

    public void activate()
    {
        this.active = true;
        makeVisible();
    }

    public void makeTransparent()
    {
        for (Shape shape : shapes) {
            try {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        shape.setFill(Color.TRANSPARENT);
                    }
                });
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Exception");
            }
        }
    }

    public void makeVisible()
    {
        for (Shape shape : shapes) {
            try {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        shape.setFill(Color.BLUE);
                        if (shape instanceof Text) {
                            shape.setFill(Color.GREEN);
                        }
                    }
                });
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Exception");
            }
        }
    }

    public Line getLine() {
        return line;
    }

    @JsonIgnore
    @Override
    public List<Shape> getShapes() {
        return shapes;
    }

    public boolean isActive()
    {
        return this.active;
    }

    public Street getVehicleStreet()
    {
        for (Street street : this.getLine().getStreets()) {
            if (street.isOnStreet(this.position)) {
                return street;
            }
        }
        return null;
    }

    public Coordinate getPosition() {
        return position;
    }

    public String getNumber() { return number; }

    @Override
    public String toString() {
        return "Vehicle{" +
                "position=" + position +
                ", number=" + number +
                '}';
    }

    public int getStopTimeIndex() {
        return stopTimeIndex;
    }

    public List<Stop> getPassedStops() {
        return passedStops;
    }

    public void setPassedStops(List<Stop> stops) {
        this.passedStops = stops;
    }

    @Override
    public void update(LocalTime time) {

    }

    public void transformShape(Coordinate coordinate) {
        for (Shape shape : shapes) {
            try {
                Platform.runLater(new Runnable() {
                    @Override public void run() {
                        shape.relocate(coordinate.getX() - CIRCLE_RADIUS, coordinate.getY() - CIRCLE_RADIUS / 2);
                        if (shape instanceof Text) {
                            shape.relocate(coordinate.getX() + 2 * CIRCLE_RADIUS, coordinate.getY());
                        }
                    }
                });
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Exception");
            }
        }
    }

    public void addPassedStop(Stop stop)
    {
        this.passedStops.add(stop);
    }

    public void setShapes(){
        shapes = new ArrayList<>();
        Circle circle = new Circle(position.getX(), position.getY(), CIRCLE_RADIUS, Color.TRANSPARENT);
        shapes.add(circle);
        Text number = new Text(position.getX() + 2 * CIRCLE_RADIUS, position.getY(), this.number);
        number.setFill(Color.TRANSPARENT);
        shapes.add(number);
        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                showLine();
                controller.DisplayDepartures();
                showSchedule();

            }
        };
        //Registering the event filter
        circle.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
    }



    public void setPosition(Coordinate position) {
        this.position = position;
    }

    private void showLine()
    {
        setSelected(true);
        controller.unhighighlightLines();
        for (Street street : this.getLine().getStreets()) {
            if(street.isHighlighted()) {
                street.setHighlighted(false);
            }
            street.Highlight();
        }

    }

    public String getDepartureTimes() {
        return DepartureTimes;
    }

    public void setDepartureTimes(String departureTimes) {
        DepartureTimes = departureTimes;
    }


    private void showSchedule() {
        DepartureTimes = "";
        for (Stop stop : this.getLine().getStops()) {
            DepartureTimes = DepartureTimes.concat(stop.getName() + "   ");
            double stopTime = stop.getStopTimes()[stopTimeIndex];
            int stopTimeHours = 0;
            if (stopTime >= 60) {
                stopTimeHours = (int) stopTime/60;
            }
            int stopTimeMinutes = (int) stopTime%60;
            String stringStopTimeHours = Integer.toString(stopTimeHours);
            String stringStopTimeMinutes = Integer.toString(stopTimeMinutes);
            if (stopTimeHours < 10) {
                stringStopTimeHours = "0".concat(stringStopTimeHours);
            }
            if (stopTimeMinutes < 10) {
                stringStopTimeMinutes = "0".concat(stringStopTimeMinutes);
            }
            String stringStopTime = stringStopTimeHours.concat(":").concat(stringStopTimeMinutes);
            DepartureTimes = DepartureTimes.concat(stringStopTime + "  ");
            DepartureTimes = DepartureTimes.concat("\n\n");
        }
        controller.getScheduleTime().setText(DepartureTimes);
    }
}
